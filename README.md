# README #

### What is this repository for? ###

* SlyTemplate is a Unity Project Template that contains a number of utilities that we find very handy.
* Version 1.0

### How do I get set up? ###

* There's almost no set up needed. The whole point is this sets everything up for you. With this template you can be up and running with Dependency injection straight away. The only requirement is dropping the Application prefab into the scene. Directions on using the Factory in your own code can be found fourther down.

### Factory ###

The RSG Factory uses reflection to auto set up dependency. Of all the systems I've tried using with Unity, this is the only one that has this auto set up. For more detailed direction on using it, check out the readme for the RSG Factory repository [https://github.com/Real-Serious-Games/Factory](https://github.com/Real-Serious-Games/Factory)
For a quick reference, you just need to do the following:

Firstly, drop the Application prefab from the Prefabs folder into the scene. This is just a GameObject with a script called UnityAppInit on it. Feel free to check that script out, it shows the factory being set up. This project is already set up so that UnityAppInit has the earliest value possible on the ScriptExecutionOrder panel. You want to do this so that you know the factory will be set up by the time any MonoBehaviours want to use it. More on that further down.

Once that's done, you can set up your own classes to be injectable like so:

```
#!c#


public interface IMyClass
{
    void DoSomething();
}

[FactoryCreatable(typeof(IMyClass))]
public class MyClass : IMyClass
{
    public void DoSomething();
}

```

Then in whichever class you want to dependency inject this into you just:


```
#!c#


[Dependency]
public IMyClass { get; set; }

```

This is called property injection. Be aware, the property will not be set till after the constructor is called. So if you need to use the class in the constructor, you'll need to use constructor injection



```
#!c#

public class MyOtherClass
{
    private IMyClass myClass;
 
    public MyOtherClass(IMyClass myClass)
    {
        //you can either just use it here
        myClass.DoSomething();

        //or if you'll need it later as well you can cache it
        this.myClass = myClass;
    } 
}

```

It's important to realise that the automatic injection will only occur for classes that are created by the factory. A MonoBehaviour, for example, will not have it's dependencies filled automatically. But it can be done. 

You'll notice UnityAppInit has the factory it creates set up as a static member, and you'll remember that we set it up in the ScriptExecutionOrder to be the first MonoBehaviour set up. So we can access the factory from any MonoBehaviour. To set up your dependencies, just do this...


```
#!c#


public class MyMonoBehaviour : MonoBehaviour
{

    [Dependency]
    public IMyDependency { get; set; }

    void Start()
    {
        UnityAppInit.Factory.ResolveDependencies(this);
    }
}
```



and you're free to use your dependencies. You can use them in Start right after that call to the factory.


The last thing to mention is Singletons. These can be very handy. The most obvious one that comes to mind for me is the Player class. How many times have you had a system or MonoBehaviour that needed to get access to something as simple as the player position. If you had it all set up as it is above, you'll get a new Player created for every class that has it dependency injected. What you need here is a Singleton. The Factory can do that (and you'll see them getting set up in UnityAppInit). To set one up, just do the following:


```
#!c#

public interface IPlayer
{
    Vector3 Position { get; }
}

[Singleton(typeof(IPlayer))]
public class Player : IPlayer
{
    Vector3 Position 
    {
        get
        {
             return transform.position;
        }
    }
}

```

and you can then have any enemy created in the game have IPlayer dependency injected just like normal and access the Position property. From the point of view of the class getting the Player injected, they don't know or care if it's a Singleton or not. Nor should they have to.

There is also the LazySingleton (set up exactly the same [LazySingleton(typeof(IPlayer))]) which I like to use. Lazy, if you haven't heard the term before, refers to an operation that doesn't happen until the first time it's needed. In this case, the Factory will not go through the process of setting up this class until the first time something asks for it to be injected. This can help with load balancing. Since the Factory uses Reflection, which can be expensive, you may not want to set every class up at load time. Or perhaps there are certain other conditions that you want met before you try to set up this Lazy class. 

I mentioned performance, but I'm using Dependency Injection quite a lot in my current project, on enemies and weapon bullets, and have seen little runtime impact on my Galaxy S5. 

### TaskManager ###

The Task Manager is a fluent alternative to using Unity Coroutines. Check out [the github repository](https://gist.github.com/nickgravelyn/2480060). It was originally written by Nick Gravelyn, I've just modified it enough to be dependency injectable with the Facotry. You can easily just use the UnityTaskManager script. Or remove the Factory attribute and set it up as a Singleton for easy access.

### RSG Promises ###

I won't go into much detail on promises here, [the documentation](https://github.com/Real-Serious-Games/C-Sharp-Promise) covers that pretty well. But basically they're a great way of handling and chaining asynchronous operations.

### Game Object Creator ###

The Game Object Creator is just a simple utility for managing procedural game objects in the scene.It will create them under a root object. It also has some functions for creating persistent game objects. You can call


```
#!c#


//create and return a game object of the given name. 
CreateGameObject (string name); 

//create and return a game object of the given name that will persist between scenes
CreatePersistentGameObject (string name);

//create and return a game object of the given name, or return the game object of that name if it already exists
CreateUniqueGameObject (string name);

//create and return a game object of the given name, or return the game object of that name if it already exists, that will persist between scenes
CreateUniquePersistentGameObject (string name);

```


It can be dependency injected and used like this:


```
#!c#

[Dependency]
public IGameObjectCreator GameObjectCreator { get; set; }

public void SomeFunction()
{
    var go = GameObjectCreator.CreateUniquePersistentGameObject("someName");
    go.AddComponent<SomeComponent>();
}
```

### Archiver ###

The archiver is again factory injectable. It's a simple wrapper for the open source C# archiver CSharpZipLib. There is a lot more functionality that could be added to this, there is no encryption happening. 

To use it, simply call 


```
#!c#

ResourceArchiver.PackZip("path_to_directory_to_zip", "path_to_save_zip.bytes");
```

Notice that I save the zip as .bytes. Unity doesn't recognise a .zip file and this class is specifically designed to unpack from a Resources.Load call. For example


```
#!c#

ResourceArchiver.UnpackZipToPersistentDataPath("Data");
```
This will, internally, specifically call Resources.Load("Data") and create a TestAsset from it. It'll then use that to write all the bytes with CSharpZipLib and recreate the directory structure that was Packed into Application.persistentDataPath. This is excellent for making sure data is where you want it to be regardless of the platform. I use a folder structure of json files that represent data for enemies and weapons and player configuration. I zip that up and put it into a Resources folder. At runtime I unpack that into the persistent data path of the platform it's running on. Then when I'm deserialising the json files I know the filepath will always be the same.

### EventBasedUIItem ###

The final little tool in the kit is this puppy. This is specifically for the new UI in Unity 4.6. When trying to set up a simple button I hit some snags. It seemed that the button only had an OnClick callback that was set up in the inspector. I wanted way more functionality from my button. So I investigated. It seemed I could use something called an EventTrigger. This is a component that I can add, which has events for what I was looking for. But again, it seemed to be set up in the inspector. I wanted something much neater. I did manage to find some code that showed how to set it up, but I didn't want to have to do that every time. So I wrote this. Using it is very simple. 

First, create a class that inherits from EventBasedUIItem. The only issue is inheriting from a monobehaviour isn't clean. Instead of using a Start or Update, which the base class is using already, you implement InheritedStart and IneritedUpdate and use them in the same way.
Then just add your new monobehaviour to whichever UI item I want events from. 


```
#!c#

protected override void InheritedUpdate()
{
    if(UIDown)
    {
        //do something
    }
}

```

or you can hook into events instead


```
#!c#

protected override void InheritedStart()
{
    base.OnPointerDown += ButtonPressed;
}

void ButtonPressed(object sender, EventArgs e)
{
    //do something
}
```

A lot neater in my opinion than the way it appears to be made to be set up. Or I was doing something wrong. Either way, this exists now and I like using it.