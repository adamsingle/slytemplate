﻿/*==== EventBasedUIItem.cs ==================================================
 * Class for easily handling simple UI states with Unity new UI
 * Original Author: Adam Single, Lead Programmer - Sly Budgie Pty Ltd.
 * Based On: An answer found on Unity Answers (http://answers.unity3d.com/questions/781726/how-do-i-add-a-listener-to-onpointerenter-ugui.html)
 * 
 * Created On: 11/1/2015 
 * 
 * USAGE:
 * ::Inherit a new class from this one. You can either monitor the states like the old UI such as
 * in InheritedUpdate() add if(UIDown){ Fire() } this will call Fire() once, the frame the UI is pressed. Or
 * if(UIHeld) which will remain true for so long as the UI element is pressed on. Note you can set StayHeldWhenNotOver to 
 * true if you want this property to remain true if the user drags off of the UI element while holding it down. By default
 * that sets the UIHeld property to false.
 * 
 * ::Alternatively you can subscribe to the events and handle the logic more manually. The choice is yours.
 *
 * =========================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Assets.Code.UI
{
    public abstract class EventBasedUIItem : MonoBehaviour
    {
        protected EventTrigger eventTrigger = null;

        /// <summary>
        /// Event fired when the pointer (click or touch) is pressed down
        /// </summary>
        protected EventHandler OnPointerDown;

        /// <summary>
        /// Event fired when the pointer (click or touch) is released
        /// </summary>
        protected EventHandler OnPointerUp;

        /// <summary>
        /// Event fired when the pointer enters the bounds of the UI.
        /// </summary>
        protected EventHandler OnPointerEnter;

        /// <summary>
        /// Event fired when the pointer leaves the bounds of the UI.
        /// </summary>
        protected EventHandler OnPointerExit;

        /// <summary>
        /// Event fired when the pointer begins a hold (same as OnPointerDown)
        /// </summary>
        protected EventHandler OnPointerHeldStarted;

        /// <summary>
        /// Event fired when the pointer ends a hold (same as OnPointerUp but also fired when the pointer exits the bounds
        /// if StayHeldWhenNotOver is false.
        /// </summary>
        protected EventHandler OnPointerHeldEnded;

        /// <summary>
        /// Possible states of a UI element - includes extra states for calcuations such as when to turn off UIUP and UIDOWN
        /// </summary>
        [Flags]
        private enum UIState
        {
            UIDOWN = 1,                 //0000 0000 0000 0000
            UIUP = 2,                   //0000 0000 0000 0001
            UIHELD = 4,                 //0000 0000 0000 0010
            UIOVER = 8,                 //0000 0000 0000 0100
            UIDOWNLASTFRAME = 16,        //0000 0000 0000 1000
            UIUPLASTFRAME = 32,         //0000 0000 0001 0000
        }

        /// <summary>
        /// The current state of this UI Element
        /// </summary>
        private UIState currentState;

        /// <summary>
        /// True the frame that the input is pressed down on
        /// </summary>
        protected bool UIDown
        {
            get
            {
                return (currentState & UIState.UIDOWN) > 0;
            }
        }

        /// <summary>
        /// True if UIDown was true last frame. Used to know when to turn it off.
        /// </summary>
        private bool UIDownLastFrame
        {
            get
            {
                return ( currentState & UIState.UIDOWNLASTFRAME ) > 0;
            }
        }

        /// <summary>
        /// True the frame that the input is released
        /// </summary>
        protected bool UIUp
        {
            get
            {
                return ( currentState & UIState.UIUP ) > 0;
            }
        }

        /// <summary>
        /// True if UIUp was true last frame. Used to know when to turn it off.
        /// </summary>
        private bool UIUpLastFrame
        {
            get
            {
                return ( currentState & UIState.UIUPLASTFRAME ) > 0;
            }
        }

        /// <summary>
        /// true when the mouse cursor hovering over the UI element
        /// </summary>
        protected bool UIOver
        {
            get
            {
                return ( currentState & UIState.UIOVER ) > 0;
            }
        }

        /// <summary>
        /// Remains true as long as an element is held down. Use StayHeldWhenNotOver to control whether held 
        /// turns false when the user drags off the UI element while holding.
        /// </summary>
        protected bool UIHeld
        {
            get
            {
                return ( currentState & UIState.UIHELD ) > 0;
            }
        }

        /// <summary>
        /// Is UIHeld true if the user drags off the button while holding
        /// </summary>
        public bool StayHeldWhenNotOver = false;

        void Start()
        {
            eventTrigger = GetComponent<EventTrigger>();

            if(eventTrigger == null)
            {
                eventTrigger = gameObject.AddComponent<EventTrigger>();
            }

            AddEventTrigger( PointerDown, EventTriggerType.PointerDown );
            AddEventTrigger( PointerUp, EventTriggerType.PointerUp );
            AddEventTrigger( PointerEntered, EventTriggerType.PointerEnter );
            AddEventTrigger( PointerExited, EventTriggerType.PointerExit );

            //Call the inherited classes Start()
            InheritedStart();
        }

        void Update()
        {
            if(UIDown && !UIDownLastFrame)
            {
                currentState |= UIState.UIDOWNLASTFRAME;
            }
            else if(UIDown && UIDownLastFrame)
            {
                currentState &= ~UIState.UIDOWN;
                currentState &= ~UIState.UIDOWNLASTFRAME;
            }
            else if(UIUp && !UIUpLastFrame)
            {
                currentState |= UIState.UIUPLASTFRAME;
            }
            else if(UIUp && UIUpLastFrame)
            {
                currentState &= ~UIState.UIUP;
                currentState &= ~UIState.UIUPLASTFRAME;
            }
            InheritedUpdate();
        }

        private void PointerExited ()
        {
            currentState &= ~UIState.UIOVER;  
            if(OnPointerExit != null)
            {
                OnPointerExit( this, new EventArgs() );
            }
            
            if ( !StayHeldWhenNotOver )
            {
                currentState &= ~UIState.UIHELD;
                if ( OnPointerHeldEnded != null )
                {
                    OnPointerHeldEnded( this, new EventArgs() );
                }
            }  
        }

        private void PointerEntered ()
        {
            currentState |= UIState.UIOVER;
            if(OnPointerEnter != null)
            {
                OnPointerEnter( this, new EventArgs() );
            }                        
        }

        private void PointerUp ()
        {
            currentState |= UIState.UIUP;
            if(OnPointerUp != null)
            {
                OnPointerUp( this, new EventArgs() );
            }
            currentState &= ~UIState.UIHELD;
            if(OnPointerHeldEnded != null)
            {
                OnPointerHeldEnded( this, new EventArgs() );
            }
        }

        private void PointerDown ()
        {
            currentState = currentState | UIState.UIDOWN;
            if(OnPointerDown != null)
            {
                OnPointerDown( this, new EventArgs() );
            }
            currentState = currentState | UIState.UIHELD;
            if(OnPointerHeldStarted != null)
            {
                OnPointerHeldStarted( this, new EventArgs() );
            }
        }

        /// <summary>
        /// Inherited classes can use this function to replace the typical Start() function of a MonoBehaviour.
        /// </summary>
        protected virtual void InheritedStart ()
        {

        }

        /// <summary>
        /// Inherited classes can use this function to replace the typical Update() function of a MonoBehaviour
        /// </summary>
        protected virtual void InheritedUpdate ()
        {

        }

        /// <summary>
        /// Add actions to the eventTrigger component to catch typical UI events
        /// </summary>
        protected void AddEventTrigger(Action action, EventTriggerType triggerType)
        {
            EventTrigger.TriggerEvent trigger = new EventTrigger.TriggerEvent();
            trigger.AddListener( (eventData) => action() );

            EventTrigger.Entry entry = new EventTrigger.Entry() { callback = trigger, eventID = triggerType };
            if ( eventTrigger.delegates == null )
            {
                eventTrigger.delegates = new List<EventTrigger.Entry>();
            }
            eventTrigger.delegates.Add( entry );
        }

        /// <summary>
        /// Add actions to the eventTrigger component to catch typical UI events
        /// </summary>
        protected void AddEventTrigger(Action<BaseEventData> action, EventTriggerType triggerType)
        {
            EventTrigger.TriggerEvent trigger = new EventTrigger.TriggerEvent();
            trigger.AddListener( (eventData) => action( eventData ) );

            EventTrigger.Entry entry = new EventTrigger.Entry() { callback = trigger, eventID = triggerType };
            if(eventTrigger.delegates == null)
            {
                eventTrigger.delegates = new List<EventTrigger.Entry>();
            }
            eventTrigger.delegates.Add( entry );
        }
    }
}
