﻿using RSG.Factory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Code.Utils
{
    public interface ITaskManager
    {
        /// <summary>
        /// Schedule a task to return after a given amount of time
        /// </summary>
        TaskQueue WaitFor (float seconds);

        /// <summary>
        /// Schedule a task to return after a given condition has been met
        /// </summary>
        TaskQueue WaitUntil (UntilTaskPredicate condition);
    }

    [LazySingleton(typeof(ITaskManager))]
    public class TaskManager : ITaskManager
    {
        private UnityTaskManager unityTaskManager;

        public TaskManager(IGameObjectCreator gameObjectCreator)
        {
            var go = gameObjectCreator.CreateUniquePersistentGameObject( "TaskManager" );
            unityTaskManager = go.AddComponent<UnityTaskManager>();
        }

        public TaskQueue WaitFor (float seconds)
        {
            return unityTaskManager.WaitFor( seconds );
        }

        public TaskQueue WaitUntil (UntilTaskPredicate condition)
        {
            return unityTaskManager.WaitUntil( condition );
        }
    }
}
