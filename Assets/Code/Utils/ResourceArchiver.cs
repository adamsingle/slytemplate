﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSharpCode.SharpZipLib.Zip;
using UnityEngine;
using System.IO;
using RSG.Factory;
using ICSharpCode.SharpZipLib.Core;

namespace Assets.Code.Utils
{
    public interface IResourceArchiver
    {
        /// <summary>
        /// Unzips a .zip file that is in Resources + filepath to the savepath
        /// </summary>
        void UnpackZip (string filepath, string savePath);

        /// <summary>
        /// A helper function that unpacks a zip to the PersistentDataPath of the platform
        /// </summary>
        void UnpackZipToPersistentDataPath (string filepath);

        /// <summary>
        /// Packs a directory structure into a zip file
        /// </summary>
        void PackZip (string directorypath, string savepath);
    }

    [FactoryCreatable(typeof(IResourceArchiver))]
    public class ResourceArchiver : IResourceArchiver
    {
        public void UnpackZipToPersistentDataPath(string filepath)
        {
            this.UnpackZip( filepath, Application.persistentDataPath );
        }

        public void UnpackZip (string filepath, string savepath)
        {
            var fullZipPathName = savepath + "/" + filepath + ".zip";
            TextAsset textAsset = Resources.Load( filepath, typeof( TextAsset ) ) as TextAsset;
            System.IO.File.WriteAllBytes( fullZipPathName, textAsset.bytes );

            ZipFile zipfile = null;
            try
            {
                FileStream fs = File.OpenRead( fullZipPathName );
                zipfile = new ZipFile( fs );

                foreach(ZipEntry entry in zipfile)
                {
                    string directoryName = Path.GetDirectoryName( entry.Name );
                    string fileName = Path.GetFileName(entry.Name);

                    if(directoryName != string.Empty)
                    {
                        Directory.CreateDirectory( Path.Combine(Application.persistentDataPath, directoryName ));
                    }

                    if(fileName != string.Empty)
                    {
                        byte[] buffer = new byte[4096]; //4k is optimal
                        Stream zipStream = zipfile.GetInputStream( entry );
                        string zipFilePath = Path.Combine(Path.Combine(Application.persistentDataPath, directoryName), fileName);
                        using(FileStream streamWriter = File.Create(zipFilePath))
                        {
                            StreamUtils.Copy( zipStream, streamWriter, buffer );
                        }
                    }
                }
            }
            finally
            {
                if(zipfile != null)
                {
                    zipfile.IsStreamOwner = true; //makes close also shut the underlying stream
                    zipfile.Close(); //ensure we release the resources
                }
            }
        }


        public void PackZip (string folderName, string outPathName)
        {
            FileStream fsOut = File.Create( outPathName );
            ZipOutputStream zipStream = new ZipOutputStream( fsOut );

            zipStream.SetLevel( 3 ); //0-9, 9 being the highest compression

            // This setting will strip the leading part of the folder path in the entries, to
            // make the entries relative to the starting folder.
            // To include the full path for each entry up to the drive root, assign folderOffset = 0.
            // To include the containing folder we need to just roll it back by the length of the folder name
            int folderOffset = folderName.Length + ( folderName.EndsWith( "\\" ) ? 0 : 1 );
            folderOffset -= Path.GetFileName( folderName ).Length + ( folderName.EndsWith( "\\" ) ? 0 : 1 );

            CompressFolder( folderName, zipStream, folderOffset );

            zipStream.IsStreamOwner = true;
            zipStream.Close();
            
        }

        private void CompressFolder (string path, ZipOutputStream zipStream, int folderOffset)
        {
            string[] files = Directory.GetFiles( path );
            foreach(var filename in files)
            {
                FileInfo inf = new FileInfo( filename );
                string entryName = filename.Substring( folderOffset );
                entryName = ZipEntry.CleanName( entryName ); //removes drive from name and fixes slash direction
                ZipEntry newEntry = new ZipEntry( entryName );
                newEntry.DateTime = inf.LastWriteTime; //note the zip format stores 2 second granularity

                // To permit the zip to be unpacked by built-in extractor in WinXP and Server2003, WinZip 8, Java, and other older code,
                // you need to do one of the following: Specify UseZip64.Off, or set the Size.
                // If the file may be bigger than 4GB, or you do not need WinXP built-in compatibility, you do not need either,
                // but the zip will be in Zip64 format which not all utilities can understand.
                //   zipStream.UseZip64 = UseZip64.Off;
                newEntry.Size = inf.Length;

                zipStream.PutNextEntry( newEntry );

                // Zip the file in buffered chunks
                // the "using" will close the stream even if an exception occurs
                byte[] buffer = new byte[4096];
                using ( FileStream streamReader = File.OpenRead( filename ) )
                {
                    StreamUtils.Copy( streamReader, zipStream, buffer );
                }
                zipStream.CloseEntry();
            }
            string[] folders = Directory.GetDirectories( path );
            foreach ( string folder in folders )
            {
                CompressFolder( folder, zipStream, folderOffset );
            }
        }
    }
}
