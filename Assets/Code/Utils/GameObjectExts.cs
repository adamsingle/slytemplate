﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Code.Utils
{
    public static class GameObjectExts
    {
        /// <summary>
        /// Find the root ancestor of the GameObject
        /// </summary>
        public static GameObject GetRootAncestor(this GameObject go)
        {
            if(go.transform.parent != null)
            {
                return go.transform.parent.gameObject.GetRootAncestor();
            }
            return go;
        }

        /// <summary>
        /// Find a child of a given name in the hierarchy beneath the GameObject
        /// </summary>
        public static GameObject FindChildInHierarchy(this GameObject go, string name)
        {
            return go.GetComponentsInChildren<Transform>()
                .Where( child => child.name == name )
                .Select( child => child.gameObject )
                .FirstOrDefault();
        }

        /// <summary>
        /// Find all children of a game object
        /// </summary>
        public static IEnumerable<GameObject> Children (this GameObject go)
        {
            foreach ( Transform child in go.transform )
            {
                yield return child.gameObject;
                child.gameObject.Children();
            }
        }
    }
}
