﻿using RSG.Factory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Code.Utils
{
    public interface IGameObjectCreator
    {
        /// <summary>
        /// Create a game object in the scene with the given name
        /// </summary>
        IGameObject CreateGameObject(string name);

        /// <summary>
        /// Create or return a procedural game object with the given name. This is treated as a different instance than 
        /// a game object with the same name that has been created as persistent
        /// </summary>
        IGameObject CreateUniqueGameObject(string name);

        /// <summary>
        /// Create a game object in the scene with the given name that will persist between scenes
        /// </summary>
        IGameObject CreatePersistentGameObject(string name);

        /// <summary>
        /// Create or return a procedural game object with the given name that will persist between scenes. This is treated
        /// as a different instance than a game object with the same name that has been created as non-persistent.
        /// </summary>
        IGameObject CreateUniquePersistentGameObject(string name);
    }

    
}
