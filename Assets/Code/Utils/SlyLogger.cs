﻿using RSG.Factory;
using RSG.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Code.Utils
{
    /// <summary>
    /// A simple logger that implements ILogger. This needs a lot of work to be more
    /// usefull for metrics.
    /// </summary>
     
    [FactoryCreatable(typeof(ILogger))]
    public class SlyLogger : ILogger
    {
        public void LogError (Exception ex, string message, params object[] args)
        {
            Debug.LogError( message );
        }

        public void LogError (string message, params object[] args)
        {
            Debug.LogError( message );
        }

        public void LogInfo (string message, params object[] args)
        {
            Debug.Log( message );
        }

        public void LogVerbose (string message, params object[] args)
        {
            Debug.Log( message );
        }

        public void LogWarning (string message, params object[] args)
        {
            Debug.LogWarning( message );
        }
    }
}
