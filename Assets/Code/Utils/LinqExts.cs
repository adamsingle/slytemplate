﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Code.Utils
{
    public static class LinqExts
    {
        public static IEnumerable<T> Each<T> (this IEnumerable<T> source, Action<T> fn)
        {
            foreach ( var item in source )
            {
                if ( item != null )
                {
                    fn.Invoke( item );
                }
            }
            return source;
        }
    }
}
