﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Code.Utils
{
    public interface IGameObject
    {
        /// <summary>
        /// Add a component to the game object by generic type 
        /// </summary>
        T AddComponent<T>() where T : UnityEngine.Component;

        /// <summary>
        /// Add a component to the game object by name returning a base component type
        /// </summary>
        UnityEngine.Component AddComponent(string componentName);
    }
}
