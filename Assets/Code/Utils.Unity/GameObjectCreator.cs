﻿using RSG.Factory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Code.Utils.Unity
{
    [LazySingleton(typeof(IGameObjectCreator))]
    public class GameObjectCreator : IGameObjectCreator
    {
        /// <summary>
        /// The name of the root game object created in the scene for all procedural game objects that aren't persistent
        /// </summary>
        static readonly string ProceduralGameObjectName = "_ProceduralGameObjects";

        /// <summary>
        /// The name of the root game object created for all persistent game objects
        /// </summary>
        static readonly string PersistentProceduralRootObjectName = "_PersistentProceduralGameObjects";

        /// <summary>
        /// The root game object for all procedurally created game objects that aren't persistent
        /// </summary>
        private GameObject proceduralRoot;

        /// <summary>
        /// The root game object for all procedurally created game objects that are persistent
        /// </summary>
        private GameObject persistentRoot;

        public GameObjectCreator()
        {
            proceduralRoot = new GameObject(ProceduralGameObjectName);
            persistentRoot = new GameObject(PersistentProceduralRootObjectName);
        }

        public IGameObject CreateGameObject(string name)
        {
            CheckName(name);

            var go = new GameObject(name);
            go.transform.parent = proceduralRoot.transform;
            return go.Wrap();
        }

        public IGameObject CreateUniqueGameObject(string name)
        {
            CheckName(name);
            var go = proceduralRoot
                .Children()
                .Where(child => child.name == name)
                .FirstOrDefault();

            if (go == null)
            {
                return CreateGameObject(name);
            }

            return go.Wrap();
        }

        public IGameObject CreatePersistentGameObject(string name)
        {
            CheckName(name);

            var go = new GameObject(name);
            go.transform.parent = persistentRoot.transform;
            GameObject.DontDestroyOnLoad(go);

            return go.Wrap();
        }

        public IGameObject CreateUniquePersistentGameObject(string name)
        {
            CheckName(name);

            var go = persistentRoot
                .Children()
                .Where(child => child.name == name)
                .FirstOrDefault();

            if (go == null)
            {
                return CreatePersistentGameObject(name);
            }

            return go.Wrap();
        }

        private void CheckName(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ApplicationException("[GameObjectCreator]: CreateGameObject() called with an empty or null name");
            }
        }
    }
}
