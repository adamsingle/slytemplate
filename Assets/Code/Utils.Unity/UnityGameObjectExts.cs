﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Code.Utils.Unity
{
    public static partial class UnityGameObjectExts
    {
        public static GameObject Unwrap(this IGameObject go)
        {
            return ((UnityGameObject)go)._gameObject;
        }

        public static IGameObject Wrap(this GameObject go)
        {
            var igo = go.GetComponent<UnityGameObject>();
            if(igo == null)
            {
                igo = go.AddComponent<UnityGameObject>();
                igo._gameObject = go;
            }

            return igo;
        }
    }
}
