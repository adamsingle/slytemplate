﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Code.Utils.Unity
{
    public class UnityGameObject : MonoBehaviour, IGameObject
    {
        public GameObject _gameObject;

        /// <summary>
        /// Add a component to the game object by generic type 
        /// </summary>
        public T AddComponent<T>() where T : Component
        {
            return _gameObject.AddComponent<T>();
        }

        /// <summary>
        /// Add a component to the game object by name returning a base component type
        /// </summary>
        public Component AddComponent(string componentName)
        {
            return _gameObject.AddComponent(componentName);
        }
    }
}
