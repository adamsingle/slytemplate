﻿using Assets.Code.Utils;
using RSG.Factory;
using RSG.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Code
{
    public class UnityAppInit : MonoBehaviour
    {
        public static Factory Factory;

        void Start()
        {
            var logger = new SlyLogger();
            Factory = new Factory( "Factory", logger );

            Factory.AutoRegisterTypes();

            var reflection = new Reflection();
            var singletonManager = new SingletonManager( reflection, logger, Factory );
            var singletonScanner = new SingletonScanner( reflection, logger, singletonManager );

            Factory.AddDependencyProvider( singletonManager );

            singletonScanner.ScanSingletonTypes();
        }
    }
}
